package javatraining.homework4.model;

public class WundergroundDto {
	
	private WundergroundDtoObservation current_observation;

	public WundergroundDtoObservation getCurrent_observation() {
		return current_observation;
	}

	public void setCurrent_observation(WundergroundDtoObservation current_observation) {
		this.current_observation = current_observation;
	}
	
	
}
