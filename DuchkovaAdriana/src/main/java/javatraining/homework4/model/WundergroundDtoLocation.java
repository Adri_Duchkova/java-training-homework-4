package javatraining.homework4.model;
 
public class WundergroundDtoLocation {
	private LocationCity city;

	public LocationCity getCity() {
		return city;
	}

	public void setCity(LocationCity city) {
		this.city = city;
	}
	
}
