package javatraining.homework4.model;
/**
 * particular items in current observation JSON object which are requested
  */
public class WundergroundDtoObservation {
	private WundergroundDtoLocation display_location;
	private String local_time_rfc822;
	private Double temp_c;
	private String relative_humidity;
	private Double wind_kph; 
	private String wind_dir;
	private String wind_string;
	private String weather;
	
	
	public WundergroundDtoLocation getDisplay_location() {
		return display_location;
	}
	public void setDisplay_location(WundergroundDtoLocation display_location) {
		this.display_location = display_location;
	}
	public String getLocal_time_rfc822() {
		return local_time_rfc822;
	}
	public void setLocal_time_rfc822(String local_time_rfc822) {
		this.local_time_rfc822 = local_time_rfc822;
	}
	public Double getTemp_c() {
		return temp_c;
	}
	public void setTemp_c(Double temp_c) {
		this.temp_c = temp_c;
	}
	public String getRelative_humidity() {
		return relative_humidity;
	}
	public void setRelative_humidity(String relative_humidity) {
		this.relative_humidity = relative_humidity;
	}
	public Double getWind_kph() {
		return wind_kph;
	}
	public void setWind_kph(Double wind_kph) {
		this.wind_kph = wind_kph;
	}
	public String getWind_dir() {
		return wind_dir;
	}
	public void setWind_dir(String wind_dir) {
		this.wind_dir = wind_dir;
	}
	public String getWind_string() {
		return wind_string;
	}
	public void setWind_string(String wind_string) {
		this.wind_string = wind_string;
	}
	public String getWeather() {
		return weather;
	}
	public void setWeather(String weather) {
		this.weather = weather;
	}
	
}
