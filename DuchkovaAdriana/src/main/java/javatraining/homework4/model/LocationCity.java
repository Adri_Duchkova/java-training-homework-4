package javatraining.homework4.model;

	public enum LocationCity{ 
			Ostrava("Czech_Republic"),
			Vilnius("Lithuania"),
			Oslo("Norway"),
			Stockholm("Sweden"),
			Helsinki("Finland");
			
	private String country;
	private LocationCity(String c) {
		country = c;
		}
			 
	 public String getCountry() {
		return country;
	 }
	
}
