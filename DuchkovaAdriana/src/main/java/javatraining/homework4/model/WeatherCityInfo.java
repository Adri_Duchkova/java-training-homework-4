package javatraining.homework4.model;

public class WeatherCityInfo {
	
	private LocationCity location;
	private String timestamp;
	private Double temp_celsius;
	private String relative_humidity;
	private Double windSpeed;
	private String windDirection;
	private String windDescription;
	private String weatherDescription;
	
	public LocationCity getLocation() {
		return location;
	}
	public void setLocation(LocationCity location) {
		this.location = location;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public Double getTempCelsius() {
		return temp_celsius;
	}
	public void setTempCelsius(Double temp_celsius) {
		this.temp_celsius = temp_celsius;
	}
	
	public Double getWindSpeed() {
		return windSpeed;
	}
	public void setWindSpeed(Double windSpeed) {
		this.windSpeed = windSpeed;
	}
	public String getWindDirection() {
		return windDirection;
	}
	public void setWindDirection(String windDirection) {
		this.windDirection = windDirection;
	}
	public String getWindDescription() {
		return windDescription;
	}
	public void setWindDescription(String windDescription) {
		this.windDescription = windDescription;
	}
	public String getWeatherDescription() {
		return weatherDescription;
	}
	public void setWeatherDescription(String weatherDescription) {
		this.weatherDescription = weatherDescription;
	}
	public String getRelative_humidity() {
		return relative_humidity;
	}
	public void setRelative_humidity(String relative_humidity) {
		this.relative_humidity = relative_humidity;
	}
	
    
}
