package javatraining.homework4.restcontroller;
import java.net.URISyntaxException;
import java.util.Collection;
import javatraining.homework4.model.LocationCity;
import javatraining.homework4.model.WeatherCityInfo;
import javatraining.homework4.service.WeatherService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
/**
 * handles HTTP request to build RESTful web service
 *
 *
 */
@RestController
public class WeatherCityController {

private WeatherService weatherservice;
public WeatherCityController(){
	weatherservice = new WeatherService();
}
/**
 * shows weather conditions in cities of Ostrava, Helsinki, Oslo, Vilnius and Stockholm, 
 * ensures HTTP requests to /weather are mapped to the getWeather() method
 * @return collection of data
 * @throws URISyntaxException to indicate that a string could not be parsed as a URI reference
 */
@RequestMapping("/weather")
public Collection<WeatherCityInfo> getWeather() throws URISyntaxException {
	return weatherservice.getWeatherAllCities();
	}
/**
 * shows weather conditions in city specified after /weather/
 * ensures HTTP requests to /weather/{city} are mapped to the getWeatherCity() method
 * @param one city of the list
 * @return conditions in the city
 * @throws URISyntaxException to indicate that a string could not be parsed as a URI reference
 */
@RequestMapping("/weather/{city}")
public WeatherCityInfo getWeatherCity(@PathVariable LocationCity city) throws URISyntaxException {
	return weatherservice.getWeatherOneCity(city);
	}

}