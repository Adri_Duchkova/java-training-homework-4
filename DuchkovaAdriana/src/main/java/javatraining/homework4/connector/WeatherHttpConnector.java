package javatraining.homework4.connector;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import javatraining.homework4.model.*;

public class WeatherHttpConnector {
	private static String BASE_URL = "http://api.wunderground.com/api/52312b2d92915c09/conditions/q/";

	public WundergroundDto getWeatherCity(LocationCity chosenCity) {
		final String uri = BASE_URL + chosenCity.getCountry() + "/" + chosenCity.toString()+ ".json";
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity <WundergroundDto> result = restTemplate.getForEntity(uri, WundergroundDto.class);
			return result.getBody();
		}	
}
