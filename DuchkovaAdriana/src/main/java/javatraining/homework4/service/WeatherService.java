package javatraining.homework4.service;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javatraining.homework4.connector.WeatherHttpConnector;
import javatraining.homework4.model.LocationCity;
import javatraining.homework4.model.WeatherCityInfo;
import javatraining.homework4.model.WundergroundDto;

public class WeatherService {
		
private WeatherHttpConnector connector;
public WeatherService(){
	connector = new WeatherHttpConnector();
	}
/** 
 * @return collection of information for every city in LocationCity
 * @throws URISyntaxException
 */
public Collection<WeatherCityInfo> getWeatherAllCities() throws URISyntaxException{
	List<WeatherCityInfo> allCities = new ArrayList<>();
	//cycle for every city of LocationCity
	for(LocationCity city: LocationCity.values()){
		WeatherCityInfo oneCity = getWeatherOneCity(city);
		allCities.add(oneCity);
		}
	return allCities;
}
/**
 * takes info from JSON list and transform to WeatherCityInfo format
 * @param city of the LocationCity list
 * @return
 * @throws URISyntaxException
 */
public WeatherCityInfo getWeatherOneCity(LocationCity city) throws URISyntaxException{
	WundergroundDto dataWunderground = connector.getWeatherCity(city);
	return transformToInfoFormat(dataWunderground);
	}

private WeatherCityInfo transformToInfoFormat(WundergroundDto dataWunderground){
	WeatherCityInfo dataInfo = new WeatherCityInfo();
	dataInfo.setLocation(dataWunderground.getCurrent_observation().getDisplay_location().getCity());
	dataInfo.setTimestamp(dataWunderground.getCurrent_observation().getLocal_time_rfc822());
	dataInfo.setTempCelsius(dataWunderground.getCurrent_observation().getTemp_c());
	dataInfo.setRelative_humidity(dataWunderground.getCurrent_observation().getRelative_humidity());;
	dataInfo.setWindSpeed(dataWunderground.getCurrent_observation().getWind_kph());
	dataInfo.setWindDirection(dataWunderground.getCurrent_observation().getWind_dir());
	dataInfo.setWeatherDescription(dataWunderground.getCurrent_observation().getWeather());
	dataInfo.setWindDescription(dataWunderground.getCurrent_observation().getWind_string());
	return dataInfo;
	}
}
